import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons



'''
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
t = np.arange(0.0, 1.0, 0.001)
a0 = 5
f0 = 3
delta_f = 5.0
s = a0 * np.sin(2 * np.pi * f0 * t)
l, = plt.plot(t, s, lw=2)
ax.margins(x=0)
'''

fig, ax = plt.subplots()
plt.subplots_adjust(left=0.05, bottom=0.05, right=0.45, top=0.95)
x = np.array([0,1,2,3,4,5])
v1 = 5
v2 = 4
v3 = 3
v4 = 2
v5 = 1
y1_init = 0
y2_init = 0
y3_init = 0
y4_init = 0
delta_y = 0.01
y = np.array([0, y1_init, y2_init, y3_init, y4_init, 10])
l, = plt.plot(x, y, lw=2, color='red')
ax.margins(x=0)
plt.title('Find the fastest path from (0,0) to (5,10)!')
plt.text(0,-0.5,'speed=' + str(v1), fontsize=8)
plt.text(1,-0.5,'speed=' + str(v2), fontsize=8)
plt.text(2,-0.5,'speed=' + str(v3), fontsize=8)
plt.text(3,-0.5,'speed=' + str(v4), fontsize=8)
plt.text(4,-0.5,'speed=' + str(v5), fontsize=8)
array_data = l.get_data()
t1 = np.sqrt(1 + (array_data[1][1] - array_data[1][0])**2) / v1
t2 = np.sqrt(1 + (array_data[1][2] - array_data[1][1])**2) / v2
t3 = np.sqrt(1 + (array_data[1][3] - array_data[1][2])**2) / v3
t4 = np.sqrt(1 + (array_data[1][4] - array_data[1][3])**2) / v4
t5 = np.sqrt(1 + (array_data[1][5] - array_data[1][4])**2) / v5
t = t1 + t2 + t3 + t4 + t5
t1_text = plt.text(5.3, 6.5, 'time (x=0 --> x=1) = ' + str(t1), fontsize=10)
t2_text = plt.text(5.3, 5.5, 'time (x=1 --> x=2) = ' + str(t2), fontsize=10)
t3_text = plt.text(5.3, 4.5, 'time (x=2 --> x=3) = ' + str(t3), fontsize=10)
t4_text = plt.text(5.3, 3.5, 'time (x=3 --> x=4) = ' + str(t4), fontsize=10)
t5_text = plt.text(5.3, 2.5, 'time (x=4 --> x=5) = ' + str(t5), fontsize=10)
t_text = plt.text(5.3, 1, 'TOTAL TIME = ' + str(t), fontsize=10)


'''
axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
axamp = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
'''

axcolor = 'lightgoldenrodyellow'
ax_y1 = plt.axes([0.55, 0.85, 0.35, 0.03], facecolor=axcolor)
ax_y2 = plt.axes([0.55, 0.80, 0.35, 0.03], facecolor=axcolor)
ax_y3 = plt.axes([0.55, 0.75, 0.35, 0.03], facecolor=axcolor)
ax_y4 = plt.axes([0.55, 0.70, 0.35, 0.03], facecolor=axcolor)


ax.axvspan(0, 1, facecolor='azure', alpha=0.5)
ax.axvspan(1, 2, facecolor='lightcyan', alpha=0.5)
ax.axvspan(2, 3, facecolor='paleturquoise', alpha=0.5)
ax.axvspan(3, 4, facecolor='mediumturquoise', alpha=0.5)
ax.axvspan(4, 5, facecolor='darkturquoise', alpha=0.5)


'''
sfreq = Slider(axfreq, 'Freq', 0.1, 30.0, valinit=f0, valstep=delta_f)
samp = Slider(axamp, 'Amp', 0.1, 10.0, valinit=a0)
'''

s1 = Slider(ax_y1, 'Y1', 0, 10, valinit=y1_init)
s2 = Slider(ax_y2, 'Y2', 0, 10, valinit=y2_init)
s3 = Slider(ax_y3, 'Y3', 0, 10, valinit=y3_init)
s4 = Slider(ax_y4, 'Y4', 0, 10, valinit=y4_init)



'''
def update(val):
    amp = samp.val
    freq = sfreq.val
    l.set_ydata(amp*np.sin(2*np.pi*freq*t))
    fig.canvas.draw_idle()
'''

def update(val):
    y1 = s1.val
    y2 = s2.val
    y3 = s3.val
    y4 = s4.val
    l.set_ydata(np.array([0, y1, y2, y3, y4, 10]))
    array_data = l.get_data()
    t1 = np.sqrt(1 + (array_data[1][1] - array_data[1][0])**2) / v1
    t2 = np.sqrt(1 + (array_data[1][2] - array_data[1][1])**2) / v2
    t3 = np.sqrt(1 + (array_data[1][3] - array_data[1][2])**2) / v3
    t4 = np.sqrt(1 + (array_data[1][4] - array_data[1][3])**2) / v4
    t5 = np.sqrt(1 + (array_data[1][5] - array_data[1][4])**2) / v5
    t = t1 + t2 + t3 + t4 + t5
    t1_text.set_text('time (x=0 --> x=1) = ' + str(t1))
    t2_text.set_text('time (x=1 --> x=2) = ' + str(t2))
    t3_text.set_text('time (x=2 --> x=3) = ' + str(t3))
    t4_text.set_text('time (x=3 --> x=4) = ' + str(t4))
    t5_text.set_text('time (x=4 --> x=5) = ' + str(t5))
    t_text.set_text('TOTAL TIME = ' + str(t))
    fig.canvas.draw_idle()



'''
sfreq.on_changed(update)
samp.on_changed(update)
'''

s1.on_changed(update)
s2.on_changed(update)
s3.on_changed(update)
s4.on_changed(update)



'''
resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')
'''
resetax = plt.axes([0.8, 0.10, 0.1, 0.04])
button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')


def reset(event):
    s1.reset()
    s2.reset()
    s3.reset()
    s4.reset()
button.on_clicked(reset)

'''
rax = plt.axes([0.025, 0.5, 0.15, 0.15], facecolor=axcolor)
radio = RadioButtons(rax, ('red', 'blue', 'green'), active=0)
'''

def colorfunc(label):
    l.set_color(label)
    fig.canvas.draw_idle()
'''
radio.on_clicked(colorfunc)
'''

plt.show()
